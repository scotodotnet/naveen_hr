﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for SalaryMasterEpayClass
/// </summary>
public class SalaryMasterEpayClass
{
	public SalaryMasterEpayClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string _Ccode;

    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }

    private string _Lcode;

    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }


    private string _TokenNo;

    public string TokenNo
    {
        get { return _TokenNo; }
        set { _TokenNo = value; }
    }

    private string _Basic;

    public string Basic
    {
        get { return _Basic; }
        set { _Basic = value; }
    }

    private string _Allowance1;

    public string Allowance1
    {
        get { return _Allowance1; }
        set { _Allowance1 = value; }
    }

    private string _Allowance2;

    public string Allowance2
    {
        get { return _Allowance2; }
        set { _Allowance2 = value; }
    }
    
    private string _Deduction1;

    public string Deduction1
    {
        get { return _Deduction1; }
        set { _Deduction1 = value; }
    }

    private string _Deduction2;

    public string Deduction2
    {
        get { return _Deduction2; }
        set { _Deduction2 = value; }
    }
    
    private string _BaseSalary;

    public string BaseSalary
    {
        get { return _BaseSalary; }
        set { _BaseSalary = value; }
    }


    private string _PFSalary;

    public string PFSalary
    {
        get { return _PFSalary; }
        set { _PFSalary = value; }
    }

}
