﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class CommisionRecovery_Main : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR | Commision Voucher";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Menu_Commision"));
            //li.Attributes.Add("class", "has-sub active open");
        }
        CommisonRecoveryDetails();
    }
    protected void GridEditEntryClick(object sender, CommandEventArgs e)
    {
        Session["TransID"] = e.CommandName.ToString();
        Response.Redirect("CommisionRecovery.aspx");
    }
    public void CommisonRecoveryDetails()
    {

        DataTable dtDisplay = new DataTable();
        string Query = "select TransID,TransDate,TokenNo,ReferalType,ReferalName,Amount from CommisionRecovery_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
        dtDisplay = objdata.RptEmployeeMultipleDetails(Query);

        //dtdDisplay = objdata.EmployeeDisplay(SessionCcode,SessionLcode);
        if (dtDisplay.Rows.Count > 0)
        {
            Repeater1.DataSource = dtDisplay;
            Repeater1.DataBind();
        }
    }
    protected void lbtnAdd_Click(object sender, EventArgs e)
    {
        Session.Remove("TransID");
        Response.Redirect("CommisionRecovery.aspx");
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {

        string Query = "Delete from CommisionRecovery_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and TransID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(Query);

        string SSQL = "Delete From Commission_Transaction_Ledger where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and TransID='" + e.CommandName.ToString() + "' and FormType='CommissionRecovery'";
        objdata.RptEmployeeMultipleDetails(SSQL);

    }
}
