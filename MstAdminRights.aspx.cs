﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MstAdminRights : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Admin Rights";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;
        string Salary = "";
        string Manual = "";
        string Medical = "";

        if (chkSalaryConfirm.Checked == true)
        {
            Salary = "Yes";
        }
        else
        {
            Salary = "No";
        }

        if (chkManualAttnd.Checked == true)
        {
            Manual = "Yes";
        }
        else
        {
            Manual = "No";
        }

        if (chkMedical.Checked == true)
        {
            Medical = "Yes";
        }
        else
        {
            Medical = "No";
        }

        if (!ErrFlag)
        {
            query = "select * from MstAdminRights where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' And UserCode='" + txtUserType.SelectedValue + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count > 0)
            {
                SaveMode = "Update";
                query = "delete from MstAdminRights where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' And UserCode='" + txtUserType.SelectedValue + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            else
            {
                SaveMode = "Insert";
            }

            query = "Insert into MstAdminRights (CompCode,LocCode,UserCode,UserType,Salary,Manual,Medical)";
            query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + txtUserType.SelectedValue + "','" + txtUserType.SelectedItem.Text + "',";
            query = query + "'" + Salary + "','" + Manual + "','" + Medical + "')";
            objdata.RptEmployeeMultipleDetails(query);

            if (SaveMode == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Updated Successfully...!');", true);
            }
            else if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully...!');", true);
            }
            Clear_All_Field();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtUserType.SelectedValue = "0";
        chkSalaryConfirm.Checked = false;
        chkManualAttnd.Checked = false;
        chkMedical.Checked = false;
    }

}
