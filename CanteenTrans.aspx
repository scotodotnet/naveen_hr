﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="CanteenTrans.aspx.cs" Inherits="CanteenTrans" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
               }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Salary Process</a></li>
				<li class="active">Canteen</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Canteen </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Canteen</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Trans ID</label>
								  <asp:TextBox runat="server" ID="txtTransID" class="form-control">
								  </asp:TextBox>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>Date</label>
								  <asp:TextBox runat="server" ID="txtDate" class="form-control datepicker" ></asp:TextBox>
								 
								</div>
                               </div>
                              <!-- end col-4 -->
                              
                             <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>Food Type</label>
								  <asp:DropDownList runat="server" ID="txtType" class="form-control select2">
								  <asp:ListItem Value="All">All</asp:ListItem>
								  <asp:ListItem Value="BreakFast">BreakFast</asp:ListItem>
								  <asp:ListItem Value="Lunch">Lunch</asp:ListItem>
								  <asp:ListItem Value="Dinner">Dinner</asp:ListItem>
								  </asp:DropDownList>
							   </div>
                             </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Option</label>
								
								    <asp:RadioButtonList ID="RdbOpt" runat="server" RepeatColumns="2" class="form-control">
                                         <asp:ListItem Selected="true" Text="Daily" style="padding-right:40px" Value="1"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Monthly" Value="2"></asp:ListItem>
                                     </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-4 -->
                          </div>
                        <!-- end row -->
                         <!-- begin row -->
                          <div class="row">
                                 <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Month</label>
								  <asp:DropDownList runat="server" ID="ddlMonths" class="form-control select2">
								  </asp:DropDownList>
							   </div>
                             </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Year</label>
								  <asp:DropDownList runat="server" ID="ddlYear" class="form-control select2">
								  </asp:DropDownList>
							   </div>
                             </div>
                           <!-- end col-4 -->
                               <!-- begin col-4 -->
                                <div class="col-md-3">
								<div class="form-group">
								  <label>Hostel Canteen Amt</label>
								  <asp:TextBox runat="server" ID="txtHstCantAmt" class="form-control" Text="0"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                               <div class="col-md-3">
								<div class="form-group">
								  <label>Staff/Regular Canteen Amt</label>
								  <asp:TextBox runat="server" ID="txtStfRegCantAmt" class="form-control" Text="0"></asp:TextBox>
								</div>
                               </div>
                           <!-- end col-4 -->
                              </div>
                        <!-- end row -->
                    
                         <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         onclick="btnSave_Click"  />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->
                        
                        
                        
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>

