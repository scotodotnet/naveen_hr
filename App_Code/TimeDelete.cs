﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for TimeDelete
/// </summary>
public class TimeDelete
{
	public TimeDelete()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    private string _Ccode;

    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }

    private string _Lcode;

    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }


    private string _TokenNo;

    public string TokenNo
    {
        get { return _TokenNo; }
        set { _TokenNo = value; }
    }

    private string _ExistingNo;

    public string ExistingNo
    {
        get { return _ExistingNo; }
        set { _ExistingNo = value; }
    }


    private string _EmpName;

    public string EmpName
    {
        get { return _EmpName; }
        set { _EmpName = value; }
    }

    private string _AttDate;

    public string AttDate
    {
        get { return _AttDate; }
        set { _AttDate = value; }
    }
}
