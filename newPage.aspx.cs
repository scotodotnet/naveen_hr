﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Net;

public partial class newPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string fileName = Request.QueryString["fileName"];
        string[] File_check = fileName.Split('.');
        string Folder_Path = Request.QueryString["FolderPath"];
        Folder_Path = Folder_Path.Replace('|', '\\').ToString();
        string path = Path.Combine(Folder_Path, fileName);
        WebClient client = new WebClient();
        Byte[] buffer = client.DownloadData(path);
        HttpContext.Current.Response.Clear();

        if (File_check[1].ToString().ToUpper() == "PDF".ToString().ToUpper())
        {
            HttpContext.Current.Response.AddHeader("Content-Type", "application/pdf");
        }
        else
        {
            HttpContext.Current.Response.AddHeader("Content-Type", "image/jpeg");
        }
        HttpContext.Current.Response.AddHeader("Content-Disposition", "inline; filename=" + fileName);
        HttpContext.Current.Response.BinaryWrite(buffer);
        HttpContext.Current.Response.End();
    }
}
