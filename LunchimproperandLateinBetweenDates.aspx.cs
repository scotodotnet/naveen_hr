﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class LunchimproperandLateinBetweenDates : System.Web.UI.Page
{

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;

    double Final_Count;

    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    string Shift;
    double leaveCount;
    int i = 0;
    double Total_Time_get;
    DataTable mLocalDSOUT = new DataTable();
    DataTable dt = new DataTable();
    DataTable dt1 = new DataTable();
    DataTable mLocalDS = new DataTable();
    DataTable mLocalDS1 = new DataTable();
    DataTable mLocalDS2 = new DataTable();
    
    DataTable DataCells = new DataTable();
     string logtimein = "";
    string shift_Check = "";
    string endtime = "";
 int  latein_Check=0;
 int improper_Check = 0;
    string machineidEncrypt = "";
    string MachineID = "";
      DataTable dttime = new DataTable();
      DataTable dt_lateinCheck = new DataTable();
      int count1 = 0;
      int Count2 = 0;
      int Count3 = 0;
      int Count4 = 0;
      int Count5 = 0;
      int Count6 = 0;
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Lunch Latein improper Report Between Dates";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Division = Request.QueryString["Division"].ToString();
            Shift = Request.QueryString["Shift"].ToString();
            //WagesType = Request.QueryString["Wages"].ToString();

            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();



            DataCells.Columns.Add("SNo");
            DataCells.Columns.Add("DeptName");
            DataCells.Columns.Add("MachineID");
            DataCells.Columns.Add("ExistingCode");
            DataCells.Columns.Add("FirstName");
            DataCells.Columns.Add("LateIn");
            DataCells.Columns.Add("ImproperPunch");
            


            date1 = Convert.ToDateTime(FromDate);
            string dat = ToDate;
            Date2 = Convert.ToDateTime(dat);

            int daycount = (int)((Date2 - date1).TotalDays);
            int daysAdded = 0;

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
              //  AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                daycount -= 1;
                daysAdded += 1;
            }

            SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and IsActive='Yes'";


            dt = objdata.RptEmployeeMultipleDetails(SSQL);



            intK = 0;

            Present_WH_Count = 0;

            int SNo = 1;
            

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                count1 = 0;
               
                Count3 = 0;
                Count4 = 0;
                int sno = 1;
                for (int j = 0; j < daysAdded; j++)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                    string Date1 = dayy.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                    Date_Value_Str = dayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);

                    DateTime dayy1 = Convert.ToDateTime(date1.AddDays(j+1).ToShortDateString());
                    string Date12 = dayy.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                    SSQL = "";
                    SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
                    SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName from LogTime_Days LD";
                    SSQL = SSQL + " inner join Employee_Mst EM on EM.MachineID = LD.MachineID";

                    SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'and EM.IsActive='Yes' ";
                    SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "' and LD.MachineID='" + dt.Rows[i]["MachineID"].ToString() + "'";

                    if (Division != "-Select-")
                    {
                        SSQL = SSQL + " And EM.Division = '" + Division + "'";
                    }
                    if (Shift != "ALL")
                    {
                        SSQL = SSQL + " And Shift='" + Shift + "'";
                    }
                   
                        SSQL = SSQL + " And CONVERT(datetime,Attn_Date_Str,103) = CONVERT(datetime,'" + Date1 + "',103)";
                   

                    SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
                    AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);

                     if (AutoDTable.Rows.Count != 0)
                        {
                          
                            for (int iRow = 0; iRow < AutoDTable.Rows.Count; iRow++)
                            {
                                shift_Check = "";
                                shift_Check = AutoDTable.Rows[iRow]["Shift"].ToString();
                                MachineID = AutoDTable.Rows[iRow]["MachineID"].ToString();
                                machineidEncrypt = UTF8Encryption(AutoDTable.Rows[iRow]["MachineID"].ToString());


                                SSQL = "Select * from Mst_Lunch where Shift='" + shift_Check + "' and  LocCode='" + SessionLcode + "' and CompCode='" + SessionCcode + "' ";
                                dttime = objdata.RptEmployeeMultipleDetails(SSQL);
                                endtime = dttime.Rows[0]["EndTime"].ToString();

                              

                                SSQL = "";
                                SSQL = "Select * from LogTimeLunch_IN ";
                                SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
                                SSQL = SSQL + " And LocCode='" + SessionLcode + "' and MachineID='" + machineidEncrypt + "' ";
                                SSQL = SSQL + "And TimeIN >'" + dayy.ToString("yyyy/MM/dd") + " " + endtime + "'";
                                SSQL = SSQL + "And TimeIN <='" + dayy1.ToString("yyyy/MM/dd") + " " + "05:00" + "'";
                                mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

                                if (mLocalDS.Rows.Count != 0)
                                {


                                    count1 = count1 + 1;
                                   
                                }

                                SSQL = "";
                                SSQL = "Select MachineID,TimeOUT from LogTimeLunch_OUT";
                                SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
                                SSQL = SSQL + " And LocCode='" + SessionLcode + "' and MachineID='" + machineidEncrypt + "' ";
                                SSQL = SSQL + "And TimeOUT >'" + dayy.ToString("yyyy/MM/dd") + " " + "05:00" + "'";
                                SSQL = SSQL + "And TimeOUT <='" + dayy1.ToString("yyyy/MM/dd") + " " + "05:00" + "' ";
                                SSQL = SSQL + " and MachineID NOT IN (Select MachineID from LogTimeLunch_IN ";
                                SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
                                SSQL = SSQL + " And LocCode='" + SessionLcode + "' and MachineID='" + machineidEncrypt + "' ";
                                SSQL = SSQL + "And TimeIN >'" + dayy.ToString("yyyy/MM/dd") + " " + "05:00" + "'";
                                SSQL = SSQL + "And TimeIN <='" + dayy1.ToString("yyyy/MM/dd") + " " + "05:00" + "') ";
                                mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);

                                if (mLocalDS1.Rows.Count != 0)
                                {
                                    Count3 = Count3 + 1;
                                }


                                SSQL = "";
                                SSQL = "Select MachineID,TimeIN from LogTimeLunch_IN";
                                SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
                                SSQL = SSQL + " And LocCode='" + SessionLcode + "' and MachineID='" + machineidEncrypt + "' ";
                                SSQL = SSQL + "And TimeIN >'" + dayy.ToString("yyyy/MM/dd") + " " + "05:00" + "'";
                                SSQL = SSQL + "And TimeIN <='" + dayy1.ToString("yyyy/MM/dd") + " " + "05:00" + "' ";
                                SSQL = SSQL + " and MachineID NOT IN (Select MachineID from LogTimeLunch_OUT ";
                                SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
                                SSQL = SSQL + " And LocCode='" + SessionLcode + "' and MachineID='" + machineidEncrypt + "' ";
                                SSQL = SSQL + "And TimeOUT >'" + dayy.ToString("yyyy/MM/dd") + " " + "05:00" + "'";
                                SSQL = SSQL + "And TimeOUT <='" + dayy1.ToString("yyyy/MM/dd") + " " + "05:00" + "') ";
                                mLocalDS2 = objdata.RptEmployeeMultipleDetails(SSQL);

                                if (mLocalDS2.Rows.Count != 0)
                                {
                                    Count4 = Count4 + 1;
                                }

                        }


                    }
                           


                }
                SSQL = "Select * from Mst_LunchLink where LocCode='" + SessionLcode + "' and CompCode='" + SessionCcode + "' ";
                dt_lateinCheck = objdata.RptEmployeeMultipleDetails(SSQL);


                latein_Check = Convert.ToInt32(dt_lateinCheck.Rows[0]["LateIN"].ToString());
                improper_Check = Convert.ToInt32(dt_lateinCheck.Rows[0]["Improper"].ToString());

                Count5 = Count3 + Count4 ;
                if (count1 > latein_Check  || Count5 >improper_Check)
                {



                    Count6 = Count5 - improper_Check;
                    Count2=count1 -latein_Check;


                  

                    DataCells.NewRow();
                    DataCells.Rows.Add();
                    DataCells.Rows[DataCells.Rows.Count -1]["SNO"]=sno;
                    DataCells.Rows[DataCells.Rows.Count - 1]["DeptName"] = dt.Rows[i]["DeptName"].ToString();
                    DataCells.Rows[DataCells.Rows.Count - 1]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
                    DataCells.Rows[DataCells.Rows.Count - 1]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
                    DataCells.Rows[DataCells.Rows.Count - 1]["FirstName"] = dt.Rows[i]["FirstName"].ToString();
                    DataCells.Rows[DataCells.Rows.Count - 1]["LateIn"] = Count2;
                    DataCells.Rows[DataCells.Rows.Count - 1]["ImproperPunch"] = Count6;
                    sno = sno + 1;
                    Count2 = 0;
                    Count6 = 0;
            
                }
                Count5 = 0;



            }
            //SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
            //dt = objdata.RptEmployeeMultipleDetails(SSQL);
            //string name = dt.Rows[0]["CompName"].ToString();

            grid.DataSource = DataCells;
            grid.DataBind();
            string attachment = "attachment;filename=Lunch Latein and improper Report.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
          
            Response.Write("<tr Font-Bold='true' align='left'>");
            Response.Write("<td colspan='7'>");
            Response.Write("<a style=\"font-weight:bold\">DAY ATTENANCE BETWEEN DATES REPORT -" + Session["Lcode"].ToString() + "-" + FromDate + "-" + ToDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
          
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();


        }
    }
    public static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }
    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = "0";
        BALDataAccess objdata_new = new BALDataAccess();

        string query = "Select * from Employee_Mst where MachineID_Encrypt='" + encryptpwd + "'";
        DataTable DT = new DataTable();
        DT = objdata_new.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            decryptpwd = DT.Rows[0]["MachineID"].ToString();
        }
        else
        {
            query = "Select * from Employee_Mst_New_Emp where MachineID_Encrypt='" + encryptpwd + "'";
            DT = objdata_new.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                decryptpwd = DT.Rows[0]["MachineID"].ToString();
            }
            else
            {
                decryptpwd = "0";
            }
        }

        //UTF8Encoding encodepwd = new UTF8Encoding();
        //Decoder Decode = encodepwd.GetDecoder();
        //byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        //int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        //char[] decoded_char = new char[charCount];
        //Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        //decryptpwd = new String(decoded_char);
        return decryptpwd;
    }
}
