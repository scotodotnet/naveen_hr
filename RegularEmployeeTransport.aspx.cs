﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;

public partial class RegularEmployeeTransport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string Date;
    string Shift;
    string BusNo;
    string VeichleType;

    string SSQL;
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();
    DataTable dt1 = new DataTable();

    BALDataAccess objdata = new BALDataAccess();
    System.Web.UI.WebControls.DataGrid grid =
                          new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Bus Route";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Division = Request.QueryString["Division"].ToString();
            Date = Request.QueryString["Date"].ToString();
            Shift = Request.QueryString["Shift"].ToString();
            BusNo = Request.QueryString["BusNo"].ToString();
            VeichleType = Request.QueryString["Vehicles_Type"].ToString();


            if (SessionUserType == "2")
            {
                NonAdminRegularEnmployeeTransport();
            }

            else
            {
                AdminRegularEnmployeeTransport();
            }
        }
    }

    public void AdminRegularEnmployeeTransport()
    {

        DataCell.Columns.Add("SI.No");
         DataCell.Columns.Add("ExistingCode");
        DataCell.Columns.Add("FirstName");
        DataCell.Columns.Add("Department");
        DataCell.Columns.Add("DOJ");
        DataCell.Columns.Add("DOB");
       
         DataCell.Columns.Add("Age");
         DataCell.Columns.Add("Shift");
        DataCell.Columns.Add("Vehicle Type");
        DataCell.Columns.Add("Route No");
        DataCell.Columns.Add("Route Name");
        DataCell.Columns.Add("Village Place");
       
          SSQL = "select LT.ExistingCode,LT.FirstName,LT.DeptName,convert(varchar(10),EM.DOJ,103) as DOJ,convert(varchar(10),EM.BirthDate,103) as DOB, ";
                SSQL = SSQL + " EM.Age,LT.Shift,EM.Vehicles_Type,EM.BusNo,RM.RouteName,(EM.BusRoute) as VillageName from LogTime_Days LT ";
                SSQL = SSQL + " inner join  Employee_Mst EM on LT.ExistingCode=EM.ExistingCode And LT.MachineID=EM.MachineID And LT.CompCode=EM.CompCode And LT.LocCode=EM.LocCode ";
                SSQL = SSQL + " inner join RouteMst RM on RM.Type=EM.Vehicles_Type and RM.RouteNo=EM.BusNo And RM.ShiftName=LT.Shift ";
                SSQL = SSQL + " where convert(datetime,Attn_Date,103)=convert(datetime,'" +Date + "',103)";
                SSQL = SSQL + " And EM.CompCode='" + SessionCcode  + "' and EM.LocCode='" + SessionLcode  + "'";
                SSQL = SSQL + " And LT.CompCode='" + SessionCcode  + "' and LT.LocCode='" + SessionLcode  + "'";
                SSQL = SSQL + " And (EM.IsActive='Yes' or  CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103))";

        if (Division != "-Select-")
        {
            SSQL = SSQL + " and EM.Division='" + Division + "' ";
        }
        if (Shift != "ALL")
        {
            SSQL = SSQL + " and LT.Shift='" + Shift + "' ";
        }
        if (BusNo != "ALL")
        {
            SSQL = SSQL + " And EM.BusNo='" + BusNo + "'";
        }
        if (VeichleType != "ALL")
        {
            SSQL = SSQL + " And EM.Vehicles_Type='" + VeichleType  + "'";
        }
              

        SSQL = SSQL + " Order by ExistingCode Asc";


        
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count != 0)
        {
            int sno = 1;

            for (int m = 0; m < dt.Rows.Count; m++)
            {
                DataCell.NewRow();
                DataCell.Rows.Add();

               
           
    
       


                DataCell.Rows[m]["SI.No"] = sno;
                DataCell.Rows[m]["ExistingCode"] = dt.Rows[m]["ExistingCode"].ToString();
                DataCell.Rows[m]["FirstName"] = dt.Rows[m]["FirstName"].ToString();
                DataCell.Rows[m]["Department"] = dt.Rows[m]["DeptName"].ToString();
                DataCell.Rows[m]["DOJ"] = Convert.ToDateTime(dt.Rows[m]["DOJ"]).ToString("dd/MM/yyyy");
                DataCell.Rows[m]["DOB"] = Convert.ToDateTime(dt.Rows[m]["DOB"]).ToString("dd/MM/yyyy");
                DataCell.Rows[m]["Age"] = dt.Rows[m]["Age"].ToString();
                DataCell.Rows[m]["Shift"] = dt.Rows[m]["Shift"].ToString();

                DataCell.Rows[m]["Vehicle Type"] = dt.Rows[m]["Vehicles_Type"].ToString();
                DataCell.Rows[m]["Route No"] = dt.Rows[m]["BusNo"].ToString();
                DataCell.Rows[m]["Route Name"] = dt.Rows[m]["RouteName"].ToString();
                DataCell.Rows[m]["Village Place"] = dt.Rows[m]["VillageName"].ToString();
               

                sno += 1;
            }


            SSQL = "Select * from Company_Mst ";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt1.Rows[0]["CompName"].ToString();

            grid.DataSource = DataCell;
            grid.DataBind();
            string attachment = "attachment;filename=Regular Employee Transport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='11'>");
            Response.Write("<a style=\"font-weight:bold\">" + name + " - " + SessionLcode + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='11'>");
            Response.Write("<a style=\"font-weight:bold\">Regular employees shift & transport report</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='11'>");
            Response.Write("<a style=\"font-weight:bold\">ShiftDate: " +Date +"</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            for (int k = 0; k < DataCell.Columns.Count; k++)
            {

                Response.Write("<td colspan='1'>");
                Response.Write("<a style=\"font-weight:bold\">" + DataCell.Columns[k] + " </a>");

                Response.Write("</td>");

            }
            Response.Write("</tr>");

            for (int n = 0; n < DataCell.Rows.Count; n++)
            {

              
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["SI.No"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["ExistingCode"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["FirstName"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Department"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["DOJ"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["DOB"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Age"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Shift"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Vehicle Type"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Route No"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Route Name"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Village Place"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("</tr>");
            }
            Response.Write("</table>");
            //Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Found..');", true);
        }

    }
    public void NonAdminRegularEnmployeeTransport()
    {
        DataCell.Columns.Add("SI.No");
        DataCell.Columns.Add("ExistingCode");
        DataCell.Columns.Add("FirstName");
        DataCell.Columns.Add("Department");
        DataCell.Columns.Add("DOJ");
        DataCell.Columns.Add("DOB");

        DataCell.Columns.Add("Age");
        DataCell.Columns.Add("Shift");
        DataCell.Columns.Add("Vehicle Type");
        DataCell.Columns.Add("Route No");
        DataCell.Columns.Add("Route Name");
        DataCell.Columns.Add("Village Place");

        SSQL = "select LT.ExistingCode,LT.FirstName,LT.DeptName,convert(varchar(10),EM.DOJ,103) as DOJ,convert(varchar(10),EM.BirthDate,103) as DOB, ";
        SSQL = SSQL + " EM.Age,LT.Shift,EM.Vehicles_Type,EM.BusNo,RM.RouteName,(EM.BusRoute) as VillageName from LogTime_Days LT ";
        SSQL = SSQL + " inner join  Employee_Mst EM on LT.ExistingCode=EM.ExistingCode And LT.MachineID=EM.MachineID And LT.CompCode=EM.CompCode And LT.LocCode=EM.LocCode ";
        SSQL = SSQL + " inner join RouteMst RM on RM.Type=EM.Vehicles_Type and RM.RouteNo=EM.BusNo And RM.ShiftName=LT.Shift ";
        SSQL = SSQL + " where convert(datetime,Attn_Date,103)=convert(datetime,'" + Date + "',103)";
        SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "' and EM.Eligible_PF='1'";
        SSQL = SSQL + " And LT.CompCode='" + SessionCcode + "' and LT.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And (EM.IsActive='Yes' or  CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103))";

        if (Division != "-Select-")
        {
            SSQL = SSQL + " and EM.Division='" + Division + "' ";
        }
        if (Shift != "ALL")
        {
            SSQL = SSQL + " and LT.Shift='" + Shift + "' ";
        }
        if (BusNo != "ALL")
        {
            SSQL = SSQL + " And EM.BusNo='" + BusNo + "'";
        }
        if (VeichleType != "ALL")
        {
            SSQL = SSQL + " And EM.Vehicles_Type='" + VeichleType + "'";
        }


        SSQL = SSQL + " Order by ExistingCode Asc";



        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count != 0)
        {
            int sno = 1;

            for (int m = 0; m < dt.Rows.Count; m++)
            {
                DataCell.NewRow();
                DataCell.Rows.Add();







                DataCell.Rows[m]["SI.No"] = sno;
                DataCell.Rows[m]["ExistingCode"] = dt.Rows[m]["ExistingCode"].ToString();
                DataCell.Rows[m]["FirstName"] = dt.Rows[m]["FirstName"].ToString();
                DataCell.Rows[m]["Department"] = dt.Rows[m]["DeptName"].ToString();
                DataCell.Rows[m]["DOJ"] = Convert.ToDateTime(dt.Rows[m]["DOJ"]).ToString("dd/MM/yyyy");
                DataCell.Rows[m]["DOB"] = Convert.ToDateTime(dt.Rows[m]["DOB"]).ToString("dd/MM/yyyy");
                DataCell.Rows[m]["Age"] = dt.Rows[m]["Age"].ToString();
                DataCell.Rows[m]["Shift"] = dt.Rows[m]["Shift"].ToString();

                DataCell.Rows[m]["Vehicle Type"] = dt.Rows[m]["Vehicles_Type"].ToString();
                DataCell.Rows[m]["Route No"] = dt.Rows[m]["BusNo"].ToString();
                DataCell.Rows[m]["Route Name"] = dt.Rows[m]["RouteName"].ToString();
                DataCell.Rows[m]["Village Place"] = dt.Rows[m]["VillageName"].ToString();


                sno += 1;
            }


            SSQL = "Select * from Company_Mst ";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt1.Rows[0]["CompName"].ToString();

            grid.DataSource = DataCell;
            grid.DataBind();
            string attachment = "attachment;filename=Regular Employee Transport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='11'>");
            Response.Write("<a style=\"font-weight:bold\">" + name + " - " + SessionLcode + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='11'>");
            Response.Write("<a style=\"font-weight:bold\">Regular employees shift & transport report</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='11'>");
            Response.Write("<a style=\"font-weight:bold\">ShiftDate: " + Date + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            for (int k = 0; k < DataCell.Columns.Count; k++)
            {

                Response.Write("<td colspan='1'>");
                Response.Write("<a style=\"font-weight:bold\">" + DataCell.Columns[k] + " </a>");

                Response.Write("</td>");

            }
            Response.Write("</tr>");

            for (int n = 0; n < DataCell.Rows.Count; n++)
            {


                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["SI.No"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["ExistingCode"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["FirstName"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Department"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["DOJ"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["DOB"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Age"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Shift"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Vehicle Type"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Route No"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Route Name"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Village Place"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("</tr>");
            }
            Response.Write("</table>");
            //Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Found..');", true);
        }

    }
}
