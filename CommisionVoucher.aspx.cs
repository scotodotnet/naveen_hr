﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class CommisionVoucher : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Commision Voucher";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            load_TokenNo();
            Load_Trans_No();
            if (Session["TransID"] != null)
            {
                txtTransid.Text = Session["TransID"].ToString();
                txtTransid.Enabled = false;
                btnSearch_Click(sender, e);
            }
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DataTable DT = new DataTable();

        SSQL = "Select * from CommisionVoucher_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " and TransID='" + txtTransid.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {


            txtTransid.Text = DT.Rows[0]["TransID"].ToString();
            txtTransDate.Text = DT.Rows[0]["TransDate"].ToString();
            ddlTokenNo.SelectedValue = DT.Rows[0]["TokenNo"].ToString();
            txtrefferalType.Text = DT.Rows[0]["ReferalType"].ToString();
            txtrefferalName.Text = DT.Rows[0]["ReferalName"].ToString();
            txtAddress.Text = DT.Rows[0]["Address"].ToString();
            ddlPaymentMode.SelectedValue = DT.Rows[0]["PaymentMode"].ToString();
            txtCheckNo.Text = DT.Rows[0]["CheckNo"].ToString();
            txtCheckDate.Text = DT.Rows[0]["CheckDate"].ToString();
            txtAmount.Text = DT.Rows[0]["Amount"].ToString();
            txtNote.Text = DT.Rows[0]["Note"].ToString();
        }
        Commission_Payment_Details();
    }

    public void load_TokenNo()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlTokenNo.Items.Clear();
        query = "Select  ExistingCode from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and (ReferalType='Agent'or ReferalType='Parent') And IsActive='Yes' ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlTokenNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlTokenNo.DataTextField = "ExistingCode";
        ddlTokenNo.DataValueField = "ExistingCode";
        ddlTokenNo.DataBind();
    }

    protected void ddlTokenNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            if (DT.Rows[0]["ReferalType"].ToString() == "Agent")
            {
                txtrefferalType.Text = DT.Rows[0]["ReferalType"].ToString();
                txtrefferalName.Text = DT.Rows[0]["AgentName"].ToString();

                DataTable DT_Agent = new DataTable();

                query = "Select *from MstAgent where AgentName='" + txtrefferalName.Text + "'";
                DT_Agent = objdata.RptEmployeeMultipleDetails(query);

                if (DT_Agent.Rows.Count != 0)
                {
                    txtAddress.Text = DT_Agent.Rows[0]["Address"].ToString();
                }

            }
            else if (DT.Rows[0]["ReferalType"].ToString() == "Parent")
            {
                txtrefferalType.Text = DT.Rows[0]["ReferalType"].ToString();
                txtrefferalName.Text = DT.Rows[0]["RefParentsName"].ToString();
                txtAddress.Text = "";
            }
            else
            {
                txtrefferalType.Text = "";
                txtrefferalName.Text = "";
                txtAddress.Text = "";
            }
        }
        else
        {
            txtrefferalType.Text = "";
            txtrefferalName.Text = "";
            txtAddress.Text = "";
        }

        Commission_Payment_Details();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataTable DT = new DataTable();

        SSQL = "Select * from CommisionVoucher_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " and TransID='" + txtTransid.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count != 0)
        {
            //Delete OLD Record
            SSQL = "Delete from CommisionVoucher_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " and TransID='" + txtTransid.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
        }


        //Insert Commision Voucher
        SSQL = "Insert Into CommisionVoucher_Mst(CompCode,LocCode,TransID,TransDate,TokenNo,ReferalType,ReferalName,Address,PaymentMode,CheckNo,CheckDate,Amount,Note)";
        SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtTransid.Text + "','" + txtTransDate.Text + "','" + ddlTokenNo.SelectedItem.Text + "',";
        SSQL = SSQL + "'" + txtrefferalType.Text + "','" + txtrefferalName.Text + "','" + txtAddress.Text + "','" + ddlPaymentMode.SelectedItem.Text + "',";
        SSQL = SSQL + "'" + txtCheckNo.Text + "','" + txtCheckDate.Text + "','" + txtAmount.Text + "','" + txtNote.Text + "')";
        objdata.RptEmployeeMultipleDetails(SSQL);

        // Commission_Transaction_Ledger
        SSQL = "Select * from Commission_Transaction_Ledger where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'and FormType='CommisionVoucher'";
        SSQL = SSQL + "and Comm_Trans_No='" + txtTransid.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count != 0)
        {
            //Delete OLD Record
            SSQL = "Delete from Commission_Transaction_Ledger where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and FormType='CommisionVoucher'";
            SSQL = SSQL + " and Comm_Trans_No='" + txtTransid.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
        }


        //Insert Commision Voucher
        SSQL = "Insert Into Commission_Transaction_Ledger(CompCode,LocCode,TransDate,ReferalType,ReferalName,Credit,Debit,FormType,Comm_Trans_No,Token_No)";
        SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtTransDate.Text + "',";
        SSQL = SSQL + "'" + txtrefferalType.Text + "','" + txtrefferalName.Text + "','0',";
        SSQL = SSQL + "'" + txtAmount.Text + "','CommisionVoucher','" + txtTransid.Text + "','" + ddlTokenNo.SelectedItem.Text + "')";
        objdata.RptEmployeeMultipleDetails(SSQL);

        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Commission Voucher Saved Successfully..');", true);
        Clear_All_Field();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
        
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("CommmisionVoucher_Main.aspx");
    }

    private void Clear_All_Field()
    {
        txtTransid.Text = "";
        txtTransDate.Text = "";
        ddlTokenNo.SelectedValue = "-Select-";
        txtrefferalType.Text = "";
        txtrefferalName.Text = "";
        txtAddress.Text = "";
        ddlPaymentMode.SelectedValue = "0";
        txtCheckNo.Text = ""; txtCheckDate.Text = "";
        txtAmount.Text = "";
        txtNote.Text = "";
        Load_Trans_No();
        btnSave.Text = "Save";
    }

    protected void ddlPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPaymentMode.SelectedItem.Text == "Check")
        {
            txtCheckNo.Enabled = true;
            txtCheckDate.Enabled = true;
        }
        else
        {
            txtCheckNo.Enabled = false;
            txtCheckDate.Enabled = false;
        }
    }

    private void Commission_Payment_Details()
    {
        string query = "";
        string Commission_Full_Amt = "0";
        string Commission_Paid_Amt = "0";
        string Commission_Balance_Amt = "0";
        DataTable DT_Co = new DataTable();
        query = "Select isnull(sum(cast(Credit as decimal(18,2))),0) as CR,isnull(sum(cast(Debit as decimal(18,2))),0) as DR";
        query = query + " from Commission_Transaction_Ledger where Token_No='" + ddlTokenNo.SelectedValue + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT_Co = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Co.Rows.Count != 0)
        {
            Commission_Full_Amt = DT_Co.Rows[0]["CR"].ToString();
            Commission_Paid_Amt = DT_Co.Rows[0]["DR"].ToString();
        }
        Commission_Balance_Amt = (Convert.ToDecimal(Commission_Full_Amt) - Convert.ToDecimal(Commission_Paid_Amt)).ToString();
        txtCommission_Balance_Amt.Text = Commission_Balance_Amt;
        txtCommission_Paid_Amt.Text = Commission_Paid_Amt;
        txtCommission_Tot_Amt.Text = Commission_Full_Amt;
    }

    private void Load_Trans_No()
    {
        string query = "";
        string Trans_ID = "1";
        DataTable DT_Co = new DataTable();
        query = "Select * from CommisionVoucher_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' order by TransID Desc";
        DT_Co = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Co.Rows.Count != 0)
        {
            Trans_ID = (Convert.ToDecimal(DT_Co.Rows[0]["TransID"].ToString()) + Convert.ToDecimal(1)).ToString();
        }
        txtTransid.Text = Trans_ID;
    }
}
